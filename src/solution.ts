enum Winner {
  Yellow = "Yellow",
  Red = "Red",
  Draw = "Draw",
}

// I know that classes aren't overly Javascript-y and there's debate about wether or not
// we should use them however as one game is just one whole 'thing' (words failed me)
// A class seemed applicable here to me at least.
export class Game {
  pieces: string[];
  board: Array<Number[]>;
  regexCheck: RegExp = /([12])(\1{3}|(.{5}\1){3}|(.{6}\1){3}|((.{7}\1){3}))/;
  winner: Winner = Winner.Draw;
  moves: number = 0;

  constructor(pieces: string[]) {
    // Starting with the board rotated 90 degrees as it means I can do
    // indexOf(0) on a column to figure out which position is changed
    // Rather than looping.
    this.board = Array.from({ length: 7 }, () => Array(6).fill(0));
    this.pieces = pieces;
    this.setupBoard();
  }

  get boardState() {
    // Gets the board state and represents it as a string
    // e.g. "001102,002201" etc. so we can do a Regexmatch on it
    return this.board.map((col) => col.join("")).join();
  }

  public setupBoard() {
    // Using a normal for array so that if we win before the final move we can
    // use break; which can't be used in foreach;
    for (let index = 0; index < this.pieces.length; index++) {
      this.addPiece(this.pieces[index]);
      if (this.winner !== Winner.Draw) {
        break;
      }
    }
  }

  public addPiece(move: String) {
    // If we already have a full board or a winner there's no point going through this logic
    if (this.moves === 42 || this.winner !== Winner.Draw) return;

    let column: number = this.getAlphaVal(move.split("_")[0]);
    let piece = move.split("_")[1];

    // Find the index of the first piece that is still neither Red or Yellow
    let i = this.board[column].indexOf(0);

    if (i >= 0) {
      switch (piece) {
        case "Red":
          this.board[column][i] = 1;
          break;
        case "Yellow":
          this.board[column][i] = 2;
          break;
        default:
          break;
      }

      // Records how many moves have been taken.
      this.moves += 1;

      // After a piece has been added we check if there is a winner so that we can stop
      // adding pieces if going through the list at the start.
      this.checkWinner();
    }
  }

  public checkWinner(): void {
    // Performs a regexMatch on the board, the group will either be a 1 or 2 if a winner
    // or undefined if a draw.
    let result = this.boardState.match(this.regexCheck);

    if (result) {
      switch (result[1]) {
        case "1":
          this.winner = Winner.Red;
          break;
        case "2":
          this.winner = Winner.Yellow;
          break;
        default:
          break;
      }
    }
  }

  public getAlphaVal(alpha: String): number {
    // This will take the column in the format of A-G and
    // return it as an index to be used with the 2d array
    return alpha.toLowerCase().charCodeAt(0) - 97;
  }
}

export function whoIsWinner(piecesPositionList: string[]): Winner {
  let game = new Game(piecesPositionList);

  return game.winner;
}
