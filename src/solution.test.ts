import { whoIsWinner, Game } from "./solution";

test("should return the correct winner (Yellow)", () => {
  expect(
    whoIsWinner([
      "C_Yellow",
      "E_Red",
      "G_Yellow",
      "B_Red",
      "D_Yellow",
      "B_Red",
      "B_Yellow",
      "G_Red",
      "C_Yellow",
      "C_Red",
      "D_Yellow",
      "F_Red",
      "E_Yellow",
      "A_Red",
      "A_Yellow",
      "G_Red",
      "A_Yellow",
      "F_Red",
      "F_Yellow",
      "D_Red",
      "B_Yellow",
      "E_Red",
      "D_Yellow",
      "A_Red",
      "G_Yellow",
      "D_Red",
      "D_Yellow",
      "C_Red",
    ])
  ).toBe("Yellow");
});

test("Adding winning move changes result", () => {
  let piecesPositionList = ["A_Red", "A_Red", "A_Red"];

  let game = new Game(piecesPositionList);

  expect(game.winner).toBe("Draw");

  game.addPiece("A_Red");

  expect(game.winner).toBe("Red");
});

test("Stops after winning move", () => {
  let piecesPositionList = [
    "A_Red",
    "B_Yellow",
    "A_Red",
    "B_Yellow",
    "A_Red",
    "B_Yellow",
    "A_Red",
    "B_Yellow",
    "A_Red",
    "B_Yellow",
    "A_Red",
    "B_Yellow",
  ];

  let game = new Game(piecesPositionList);

  expect(game.moves).toBe(7);
});

test("should return the correct winner (Red)", () => {
  expect(
    whoIsWinner([
      "A_Yellow",
      "B_Red",
      "B_Yellow",
      "C_Red",
      "G_Yellow",
      "C_Red",
      "C_Yellow",
      "D_Red",
      "G_Yellow",
      "D_Red",
      "G_Yellow",
      "D_Red",
      "F_Yellow",
      "E_Red",
      "D_Yellow",
    ])
  ).toBe("Red");
});
test("should return the correct winner (Draw)", () => {
  expect(
    whoIsWinner(["A_Red", "B_Yellow", "A_Red", "E_Yellow", "F_Red", "G_Yellow"])
  ).toBe("Draw");
});

test("Adding piece to full column does nothing", () => {
  let piecesPositionList = [
    "A_Red",
    "A_Yellow",
    "A_Red",
    "A_Yellow",
    "A_Red",
    "A_Yellow",
    "A_Red",
    "A_Yellow",
    "A_Red",
    "A_Yellow",
    "A_Red",
    "A_Yellow",
    "A_Red",
    "A_Yellow",
  ];

  let game = new Game(piecesPositionList);

  expect(game.moves).toBe(6);
});

test("Trying to add piece to winning board does nothing", () => {
    let piecesPositionList = [
        "A_Red",
        "A_Red",
        "A_Red",
        "A_Red"
    ];

    let game = new Game(piecesPositionList);

    expect(game.moves).toBe(4);

    game.addPiece("A_Red");

    expect(game.moves).toBe(4);
})